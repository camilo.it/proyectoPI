﻿using System;
using System.Web;

namespace Datos.MiddleWares
{
    public class Security : System.Web.Services.Protocols.SoapHeader
    {
        public string Token { get; set; }
        public string AutenticationToken { get; set; }

        public bool CredencialesValidas(string Token)
        {
            try
            {
                if (Token == DateTime.Now.ToString("yyyyMMdd"))
                {
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool CredencialesValidas(Security SoapHeader)
        {
            try
            {
                if (SoapHeader == null)
                {
                    return false;
                }
                else {
                    if (!string.IsNullOrEmpty(SoapHeader.AutenticationToken))
                    {
                        return (HttpRuntime.Cache[SoapHeader.AutenticationToken] != null);
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    

    }
}
