﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Methods
{
    public static class UserMethods
    {
        public static string conexionString="";
        static SqlConnection con = null;
        static SqlCommand com = null;
        static SqlDataAdapter adapter = null;
        static SqlParameter parameter = null;
        static SqlDataReader reader = null;


        /// <summary>
        /// Inserta un nuevo usuario 
        /// siempre y cuando no se encuentre 
        /// registrado previamente
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string InsertarUsuario(Datos.Entities.User user)
        {
            string result = "";
            try
            {
                string answerConsult = "";
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spUser_InsertarUsuario", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("@Nombres", user.Nombres);
                com.Parameters.AddWithValue("@Apellidos", user.Apellidos);
                com.Parameters.AddWithValue("@Email", user.Email);
                com.Parameters.AddWithValue("@Password", user.Password);

                con.Open();
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    answerConsult = reader["result"].ToString();
                }

                switch (answerConsult)
                {
                    case "0":
                        result = "Se presento Un error al registrarse";
                        break;
                    case "1":
                        result = "Registro Exitoso, Ya puede Ingresar con su cuenta";

                        break;
                    case "2":
                        result = "El usuario ya se encuentra registrado";
                        break;


                    default:
                        break;
                }


                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally {
                con.Close();
                reader.Close();
            }
        }



        /// <summary>
        /// Valida si un usuario ya se encuentra registrado
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>True si ya esta registrado</returns>
        public static bool IsRegister(string email, string password)
        {
            bool result =false;
            try
            {
                string answerConsult = "";
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spUser_ValidarUsuario", con);
                com.CommandType = CommandType.StoredProcedure;
              
                com.Parameters.AddWithValue("@Email", email);
                com.Parameters.AddWithValue("@Password", password);

                con.Open();
                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    answerConsult = reader["result"].ToString();
                }

                if (answerConsult.Equals("1"))
                {
                    result = true;
                }


                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
                reader.Close();
            }

        }


        /// <summary>
        /// Obtine toda la informacion de un Usuario segun su email
        /// en caso que no se encuentre retorna null
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static Datos.Entities.User GetUser(string email)
        {
            Datos.Entities.User user = new Entities.User();

            try
            {
                DataTable dsConsulta = new DataTable();
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);
                con.Open();

                com = new SqlCommand("spUser_ObtenerUsuario", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("@Email", email);

                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    user.UserId =Convert.ToInt64 (reader["UserId"].ToString());
                    user.Nombres = reader["Nombres"].ToString();
                    user.Apellidos = reader["Apellidos"].ToString();
                    user.Email = reader["Email"].ToString();
                    user.RolId = int.Parse(reader["RolId"].ToString());
                    user.Status = int.Parse(reader["Status"].ToString());
                }



                return user;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                con.Close();
                reader.Close();
            }
        }



        /// <summary>
        /// Actuliza la informacion basica de un usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static string UpdateUser(Datos.Entities.User user)
        {
            try
            {
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spUser_UpdateUser", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("@UserId", user.UserId);
                com.Parameters.AddWithValue("@Nombres", user.Nombres);
                com.Parameters.AddWithValue("@Apellidos", user.Apellidos);                
                com.Parameters.AddWithValue("@Password", user.Password);

                con.Open();
                com.ExecuteNonQuery();

                return "Se actualizo la informacion correctamente";
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally {
                con.Close();
            }
        }

    }
}
