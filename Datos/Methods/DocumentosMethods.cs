﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Methods
{
    public static class DocumentosMethods
    {
        public static string conexionString = "";
        static SqlConnection con = null;
        static SqlCommand com = null;
        static SqlDataAdapter adapter = null;
        static SqlParameter parameter = null;
        static SqlDataReader reader = null;


        static Entities.dbPIEntities db = null;



        /// <summary>
        /// Inserta la informacion de un documento 
        /// cargado al servidor , a la base de datos
        /// </summary>
        /// <param name="documentos"></param>
        /// <returns></returns>
        public static Entities.Documentos InsertarDocumento(Entities.Documentos documentos)
        {
            try
            {
                if (!string.IsNullOrEmpty(documentos.Url) && documentos.ClasesId != 0)
                {
                    using (db = new Entities.dbPIEntities())
                    {
                        db.Documentos.Add(documentos);
                        db.SaveChanges();

                        return documentos;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
