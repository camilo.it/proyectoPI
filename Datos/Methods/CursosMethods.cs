﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Methods
{
    using System.Data;
    using System.Data.SqlClient;


    public static class CursosMethods
    {
        public static string conexionString = "";
        static SqlConnection con = null;
        static SqlCommand com = null;
        static SqlDataAdapter adapter = null;
        static SqlParameter parameter = null;
        static SqlDataReader reader = null;

        static Entities.dbPIEntities db = null;

        /// <summary>
        /// Retorna el listado de cursos pendientes por Revicion
        /// </summary>
        /// <returns></returns>
        public static List<Datos.Entities.Cursos> GetCursosPorRevisar()
        {
            List<Datos.Entities.Cursos> listCursos = new List<Entities.Cursos>();
            Entities.Cursos cursos = null;
            try
            {
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spCursos_ObtenerCursosPendienteVerificacion", con);
                com.CommandType = CommandType.StoredProcedure;

                con.Open();

                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    cursos = new Entities.Cursos();
                    cursos.CursoId = Convert.ToInt64(reader["CursoId"].ToString());
                    cursos.Titulo = reader["Titulo"].ToString();
                    cursos.Descripcion = reader["Descripcion"].ToString();
                    cursos.Requisitos = reader["Requisitos"].ToString();
                    try
                    {
                        cursos.Calificacion = int.Parse(reader["Calificacion"].ToString());
                    }
                    catch (Exception)
                    {
                        cursos.Calificacion = null;
                    }
                    try
                    {
                        cursos.Status = int.Parse(reader["Status"].ToString());
                    }
                    catch (Exception)
                    {                        
                    }
                    
                    cursos.Fecha_Publicacion = Convert.ToDateTime(reader["Fecha_Publicacion"].ToString());
                    cursos.UserId = Convert.ToInt64(reader["UserId"].ToString());

                    //añade la informacion del curso a la lista
                    listCursos.Add(cursos);
                }

                return listCursos;
            }
            catch (Exception ex)
            {

                listCursos = null;
                throw new Exception(ex.Message);
            }
            finally {
                reader.Close();
                con.Close();
            }

        }


        /// <summary>
        /// Obtine los datos de cursos que 
        /// concuerden con el prefijo
        /// (Utilizado para un autocompletar)
        /// </summary>
        /// <param name="pref"></param>
        /// <returns></returns>
        public static List<string> GetCursosPorIniciales(string pref)
        {
            List<string> listCursos = new List<string>();
            Entities.Cursos cursos = null;
            try
            {
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spCursos_GetCursosPorIniciales", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@pref",pref);

                con.Open();

                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    cursos = new Entities.Cursos();
                    cursos.CursoId = Convert.ToInt64(reader["CursoId"].ToString());
                    cursos.Titulo = reader["Titulo"].ToString();
                    cursos.Descripcion = reader["Descripcion"].ToString();
                    cursos.Requisitos = reader["Requisitos"].ToString();
                    try
                    {
                        cursos.Calificacion = int.Parse(reader["Calificacion"].ToString());
                    }
                    catch (Exception)
                    {
                        cursos.Calificacion = null;
                    }
                    try
                    {
                        cursos.Status = int.Parse(reader["Status"].ToString());
                    }
                    catch (Exception)
                    {
                    }

                    cursos.Fecha_Publicacion = Convert.ToDateTime(reader["Fecha_Publicacion"].ToString());
                    cursos.UserId = Convert.ToInt64(reader["UserId"].ToString());

                    //añade la informacion del curso a la lista
                    listCursos.Add(cursos.Titulo);
                }

                return listCursos;
            }
            catch (Exception ex)
            {

                listCursos = null;
                throw new Exception(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

        }



        /// <summary>
        /// Insertar un nuevo Curso
        /// </summary>
        /// <param name="curso"></param>
        /// <returns></returns>
        public static Entities.Cursos InsertarCurso(Entities.Cursos curso)
        {

            try
            {

                using (Entities.dbPIEntities db = new Entities.dbPIEntities())
                {
                    curso.Fecha_Publicacion = DateTime.Now;
                    db.Cursos.Add(curso);
                    db.SaveChanges();

                    return curso;
                }
                
                //    conexionString = clsConexion.stGetConexion();
                //con = new SqlConnection(conexionString);

                //com = new SqlCommand("spCursos_InsertarCurso", con);
                //com.CommandType = CommandType.StoredProcedure;

                //com.Parameters.AddWithValue("@Titulo",curso.Titulo);
                //com.Parameters.AddWithValue("@Descripcion", curso.Descripcion);
                //com.Parameters.AddWithValue("@Requisitos", curso.Requisitos);
                //com.Parameters.AddWithValue("@UserId", curso.UserId);
                //com.Parameters.AddWithValue("@Fecha_Publicacion", DateTime.Now);


                //con.Open();

                //com.ExecuteNonQuery();


                //return "Se inserto Correctamente";
            }
            catch (Exception ex)
            {

                
                throw new Exception(ex.Message);
            }
           

        }




    }
}
