﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.Methods
{
    public static class ClasesMethods
    {
        public static string conexionString = "";
        static SqlConnection con = null;
        static SqlCommand com = null;
        static SqlDataAdapter adapter = null;
        static SqlParameter parameter = null;
        static SqlDataReader reader = null;


        static Entities.dbPIEntities db = null;


        /// <summary>
        /// Retorna el listado de clases de un curso
        /// </summary>
        /// <param name="CursoId"></param>
        /// <returns></returns>
        public static List<Datos.Entities.Clases> GetClasesPorCursoId(long CursoId)
        {
            List<Datos.Entities.Clases> listClases = new List<Entities.Clases>();
            Entities.Clases clases = null;
            try
            {
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);

                com = new SqlCommand("spClases_ObtenerClasesPorCursoId", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("@CursoId",CursoId);

                con.Open();

                reader = com.ExecuteReader();

                while (reader.Read())
                {
                    clases = new Entities.Clases();

                    clases.ClasesId = Convert.ToInt64(reader["ClasesId"].ToString());
                    clases.CursoId = CursoId;
                    clases.LinkVideo = reader["LinkVideo"].ToString();
                    clases.Descripcion = reader["Descripcion"].ToString();
                    clases.Titulo = reader["Titulo"].ToString();
                    try
                    {
                        clases.Calificacion = int.Parse(reader["Calificacion"].ToString());
                    }
                    catch (Exception )
                    {
                    }
                    try
                    {
                        clases.Fecha_Publicacion = Convert.ToDateTime(reader["Fecha_Publicacion"].ToString());
                    }
                    catch (Exception)
                    {
                    }


                    //añade la informacion del curso a la lista
                    listClases.Add(clases);
                }

                return listClases;
            }
            catch (Exception ex)
            {

                listClases = null;
                throw new Exception(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

        }




        /// <summary>
        /// Retorna los datos de una clase
        /// </summary>
        /// <param name="clasesId"></param>
        /// <returns></returns>
        public static Datos.Entities.Clases GetClase(long clasesId)
        {
            Datos.Entities.Clases clases = new Entities.Clases();
            try
            {
                conexionString = clsConexion.stGetConexion();
                con = new SqlConnection(conexionString);
               

                com = new SqlCommand("spClases_GetClase", con);
                com.CommandType = CommandType.StoredProcedure;

                com.Parameters.AddWithValue("@clasesId", clasesId);

                con.Open();

                reader = com.ExecuteReader();

                while (reader.Read())
                {                    
                    clases.ClasesId = Convert.ToInt64(reader["ClasesId"].ToString());
                    clases.CursoId = Convert.ToInt64(reader["CursoId"].ToString());
                    clases.LinkVideo = reader["LinkVideo"].ToString();
                    clases.Descripcion = reader["Descripcion"].ToString();
                    clases.Titulo = reader["Titulo"].ToString();
                    try
                    {
                        clases.Calificacion = int.Parse(reader["Calificacion"].ToString());
                    }
                    catch (Exception)
                    {
                    }
                    try
                    {
                        clases.Fecha_Publicacion = Convert.ToDateTime(reader["Fecha_Publicacion"].ToString());
                    }
                    catch (Exception)
                    {
                    }
                    
                }

                return clases;
            }
            catch (Exception ex)
            {

                clases = null;
                throw new Exception(ex.Message);
            }
            finally
            {
                reader.Close();
                con.Close();
            }

        }




        /// <summary>
        /// Inserta una clase
        /// </summary>
        /// <param name="clases"></param>
        /// <returns></returns>
        public static Datos.Entities.Clases InsertarClase(Datos.Entities.Clases clases)
        {
            try
            {
           
                using (db = new Entities.dbPIEntities())
                {
                    db.Clases.Add(clases);
                    db.SaveChanges();

                    return clases;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }



    }
}
