USE [master]
GO
/****** Object:  Database [dbPI]    Script Date: 17/04/2018 9:55:55 p.m. ******/
CREATE DATABASE [dbPI]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbPI', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SERVERHOUSE\MSSQL\DATA\dbPI.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbPI_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SERVERHOUSE\MSSQL\DATA\dbPI_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [dbPI] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbPI].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbPI] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbPI] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbPI] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbPI] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbPI] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbPI] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbPI] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbPI] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbPI] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbPI] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbPI] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbPI] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbPI] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbPI] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbPI] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbPI] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbPI] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbPI] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbPI] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbPI] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbPI] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbPI] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbPI] SET RECOVERY FULL 
GO
ALTER DATABASE [dbPI] SET  MULTI_USER 
GO
ALTER DATABASE [dbPI] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbPI] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbPI] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbPI] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbPI] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'dbPI', N'ON'
GO
ALTER DATABASE [dbPI] SET QUERY_STORE = OFF
GO
USE [dbPI]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [dbPI]
GO
/****** Object:  User [invitado]    Script Date: 17/04/2018 9:55:55 p.m. ******/
CREATE USER [invitado] FOR LOGIN [invitado] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Clases]    Script Date: 17/04/2018 9:55:55 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clases](
	[ClasesId] [bigint] IDENTITY(1,1) NOT NULL,
	[CursoId] [bigint] NULL,
	[LinkVideo] [varchar](max) NULL,
	[Descripcion] [varchar](max) NULL,
	[Titulo] [varchar](50) NULL,
	[Calificacion] [int] NULL,
	[Fecha_Publicacion] [date] NULL,
 CONSTRAINT [PK_Clases] PRIMARY KEY CLUSTERED 
(
	[ClasesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comentarios]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comentarios](
	[ComentariosId] [bigint] IDENTITY(1,1) NOT NULL,
	[Comentario] [varchar](max) NULL,
	[ClasesId] [bigint] NULL,
	[UserId] [bigint] NULL,
 CONSTRAINT [PK_Comentarios] PRIMARY KEY CLUSTERED 
(
	[ComentariosId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cursos]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cursos](
	[CursoId] [bigint] IDENTITY(1,1) NOT NULL,
	[Titulo] [varchar](50) NULL,
	[Descripcion] [varchar](max) NULL,
	[Requisitos] [varchar](max) NULL,
	[Calificacion] [int] NULL,
	[Status] [int] NULL,
	[Fecha_Publicacion] [date] NULL,
	[UserId] [bigint] NULL,
	[VerificacionModerador] [varchar](50) NULL,
 CONSTRAINT [PK_Cursos] PRIMARY KEY CLUSTERED 
(
	[CursoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Documentos]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Documentos](
	[DocumentosId] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Url] [varchar](max) NULL,
	[ClasesId] [bigint] NULL,
 CONSTRAINT [PK_Documentos] PRIMARY KEY CLUSTERED 
(
	[DocumentosId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[RolId] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[RolId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombres] [varchar](100) NULL,
	[Apellidos] [varchar](100) NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[RolId] [int] NULL,
	[Status] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Clases] ON 

INSERT [dbo].[Clases] ([ClasesId], [CursoId], [LinkVideo], [Descripcion], [Titulo], [Calificacion], [Fecha_Publicacion]) VALUES (1, 1, N'<iframe width="560" height="315" src="https://www.youtube.com/embed/IKlmroHxDNc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>', NULL, N'Presentacion', NULL, CAST(N'2018-03-11' AS Date))
INSERT [dbo].[Clases] ([ClasesId], [CursoId], [LinkVideo], [Descripcion], [Titulo], [Calificacion], [Fecha_Publicacion]) VALUES (2, 1, N'https://youtu.be/embed/IKlmroHxDNc', N'En este vídeo nos acercamos ya al lenguaje SQL. hablamos de la procedencia del lenguaje, estándares y creamos una primera instrucción SQL que nos permite obtener información de una BBDD.', N'Introducción a SQL', NULL, CAST(N'2018-03-11' AS Date))
SET IDENTITY_INSERT [dbo].[Clases] OFF
SET IDENTITY_INSERT [dbo].[Cursos] ON 

INSERT [dbo].[Cursos] ([CursoId], [Titulo], [Descripcion], [Requisitos], [Calificacion], [Status], [Fecha_Publicacion], [UserId], [VerificacionModerador]) VALUES (1, N'SQL Basico', N'consultas , creacion de tablas , insercion , actualizacion y eliminacion de registros', N'Ninguno', NULL, 1, CAST(N'2018-03-11' AS Date), 2, NULL)
INSERT [dbo].[Cursos] ([CursoId], [Titulo], [Descripcion], [Requisitos], [Calificacion], [Status], [Fecha_Publicacion], [UserId], [VerificacionModerador]) VALUES (2, N'Excel', N'funciones de excel', N'Ninguno', NULL, 1, CAST(N'2018-03-11' AS Date), 2, NULL)
INSERT [dbo].[Cursos] ([CursoId], [Titulo], [Descripcion], [Requisitos], [Calificacion], [Status], [Fecha_Publicacion], [UserId], [VerificacionModerador]) VALUES (10002, N'PHP', N'manejo del lenguaje PHP', N'Ninguno', NULL, 1, CAST(N'2018-05-11' AS Date), 2, NULL)
SET IDENTITY_INSERT [dbo].[Cursos] OFF
SET IDENTITY_INSERT [dbo].[Rol] ON 

INSERT [dbo].[Rol] ([RolId], [Descripcion]) VALUES (1, N'Suscriptor')
INSERT [dbo].[Rol] ([RolId], [Descripcion]) VALUES (2, N'Administrador')
INSERT [dbo].[Rol] ([RolId], [Descripcion]) VALUES (3, N'Moderador')
SET IDENTITY_INSERT [dbo].[Rol] OFF
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserId], [Nombres], [Apellidos], [Email], [Password], [RolId], [Status]) VALUES (1, N'Juan camilo', N'Castillo saucedo', N'npgcamilo@gmail.com', N'123', 1, 1)
INSERT [dbo].[User] ([UserId], [Nombres], [Apellidos], [Email], [Password], [RolId], [Status]) VALUES (2, N'Moderador', NULL, N'moderador@gmail.com', N'123', 3, 1)
SET IDENTITY_INSERT [dbo].[User] OFF
ALTER TABLE [dbo].[Clases]  WITH CHECK ADD  CONSTRAINT [FK_Clases_Cursos] FOREIGN KEY([CursoId])
REFERENCES [dbo].[Cursos] ([CursoId])
GO
ALTER TABLE [dbo].[Clases] CHECK CONSTRAINT [FK_Clases_Cursos]
GO
ALTER TABLE [dbo].[Comentarios]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Clases] FOREIGN KEY([ClasesId])
REFERENCES [dbo].[Clases] ([ClasesId])
GO
ALTER TABLE [dbo].[Comentarios] CHECK CONSTRAINT [FK_Comentarios_Clases]
GO
ALTER TABLE [dbo].[Comentarios]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[Comentarios] CHECK CONSTRAINT [FK_Comentarios_User]
GO
ALTER TABLE [dbo].[Documentos]  WITH CHECK ADD  CONSTRAINT [FK_Documentos_Clases] FOREIGN KEY([ClasesId])
REFERENCES [dbo].[Clases] ([ClasesId])
GO
ALTER TABLE [dbo].[Documentos] CHECK CONSTRAINT [FK_Documentos_Clases]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Rol] FOREIGN KEY([RolId])
REFERENCES [dbo].[Rol] ([RolId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Rol]
GO
/****** Object:  StoredProcedure [dbo].[spClases_GetClase]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Retorna toda la informacion de una clase
*/

CREATE PROC [dbo].[spClases_GetClase](
@clasesId bigint
)
AS
BEGIN

SELECT 
ClasesId,
CursoId,
LinkVideo,
Descripcion,
Titulo,
Calificacion,
Fecha_Publicacion
FROM Clases
WHERE ClasesId=@clasesId
 
END
GO
/****** Object:  StoredProcedure [dbo].[spClases_InsertarClase]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Inserta una nueva clase respecto a un Curso*/
CREATE PROC [dbo].[spClases_InsertarClase]
(
@CursoId bigint,
@LinkVideo varchar(max),
@Descripcion varchar(max),
@Titulo varchar(50),
@Fecha_Publicacion date
)
AS 

INSERT INTO Clases
(
CursoId,
LinkVideo,
Descripcion,
Titulo,
Fecha_Publicacion
)
VALUES
(
@CursoId,
@LinkVideo,
@Descripcion,
@Titulo,
@Fecha_Publicacion
)
GO
/****** Object:  StoredProcedure [dbo].[spClases_ObtenerClasesPorCursoId]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spClases_ObtenerClasesPorCursoId]
  (
  @CursoId bigint
  )
  as 
  SELECT
  ClasesId,
  CursoId,
  LinkVideo,
  Descripcion,
  Titulo,
  Calificacion,
  Fecha_Publicacion
  FROM Clases
  WHERE  CursoId=@CursoId
GO
/****** Object:  StoredProcedure [dbo].[spCursos_AprobarCurso]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*Actuliza el Status de un curso a aprobado*/
CREATE PROC [dbo].[spCursos_AprobarCurso]
(
@CursoId bigint
)
AS


UPDATE Cursos set [Status]=1 WHERE CursoId=@CursoId
GO
/****** Object:  StoredProcedure [dbo].[spCursos_GetCursosPorIniciales]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROC [dbo].[spCursos_GetCursosPorIniciales](
  @pref varchar(max)
  )
  as 
  BEGIN
  SELECT top (5) *
  FROM Cursos
  WHERE (Titulo like '%'+@pref+'%' or Descripcion like '%'+@pref+'%')
  and [Status]=1 
   
  END
GO
/****** Object:  StoredProcedure [dbo].[spCursos_InsertarCurso]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spCursos_InsertarCurso]
(
@Titulo varchar(50),
@Descripcion varchar(max),
@Requisitos varchar(max),
@Fecha_Publicacion date,
@UserId bigint
)
AS 
INSERT INTO Cursos(
Titulo,
Descripcion,
Requisitos,
Fecha_Publicacion,
[Status],
UserId
)
VALUES(
@Titulo,
@Descripcion,
@Requisitos,
@Fecha_Publicacion,
0,
@UserId
)
GO
/****** Object:  StoredProcedure [dbo].[spCursos_ObtenerCursosPendienteVerificacion]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Retorna el listado de los cursos pendientes por la 
revision de un moderador
*/
CREATE PROC [dbo].[spCursos_ObtenerCursosPendienteVerificacion]
AS 
SELECT 
CursoId,
Titulo,
Descripcion,
Requisitos,
Calificacion,
[Status],
Fecha_Publicacion,
UserId
FROM Cursos
WHERE 
Cursos.VerificacionModerador is null   or 
(Cursos.VerificacionModerador <>'Aprobado' and 
Cursos.VerificacionModerador <>'Reprobado')
GO
/****** Object:  StoredProcedure [dbo].[spCursos_ObtenerCursosPorUserId]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Retorna todos los cursos subidos por un usuario
*/
create proc [dbo].[spCursos_ObtenerCursosPorUserId]
(
@UserId bigint
)
as 
select CursoId,Titulo
from Cursos
where UserId=@UserId
GO
/****** Object:  StoredProcedure [dbo].[spCursos_ReprobarCurso]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Actualiza el Status de un curso como reprobado =2
*/
create proc [dbo].[spCursos_ReprobarCurso]
(
@CursoId bigint
)
AS
UPDATE Cursos set [Status]=2 where CursoId=@CursoId
GO
/****** Object:  StoredProcedure [dbo].[spRol_InsertarRol]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[spRol_InsertarRol]
(
@Descripcion varchar(50)
)
AS
INSERT INTO Rol
(
Descripcion
)
values
(
@Descripcion
)
GO
/****** Object:  StoredProcedure [dbo].[spUser_InsertarUsuario]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Inserta un nuevo usuario si el Email no se encuentra a un en la base de datos
si inserta con exito retorna 1
si el email ya existe retorna 2
si se presenta algun error retorna 0
*/
CREATE PROC [dbo].[spUser_InsertarUsuario]
(
@Nombres varchar(100),
@Apellidos varchar(100),
@Email varchar(100),
@Password varchar(50)
)
AS 

DECLARE @result int;

IF NOT EXISTS(SELECT * FROM [User] WHERE [User].Email=@Email)
		BEGIN 
		   INSERT INTO [User](
		   Nombres,
		   Apellidos,
		   Email,
		   [Password],
		   RolId,
		   [Status]
		   )
		   VALUES
		   (
		   @Nombres,
		   @Apellidos,
		   @Email,
		   @Password,
		   1,
		   1
		   )

		   SET @result=1;
		END
ELSE 
BEGIN
   SET @result=2;
END 

IF @@ERROR !=0 
 BEGIN 
   SET @result=0;
 END

SELECT @result as result
GO
/****** Object:  StoredProcedure [dbo].[spUser_ObtenerUsuario]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Obtiene toda la informacion de un usuario segun su correo electronico*/
CREATE PROC [dbo].[spUser_ObtenerUsuario]
(
@Email varchar(50)
)
AS 
SELECT UserId as UserId,
Nombres as Nombres,
Apellidos as Apellidos,
Email as Email,
[Password] as Password,
RolId as RolId,
[Status] as Status FROM [User] WHERE [User].Email=@Email


GO
/****** Object:  StoredProcedure [dbo].[spUser_UpdateUser]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Permite actualizar la informacion basica de un usuario
*/
CREATE PROC [dbo].[spUser_UpdateUser](
@UserId bigint,
@Nombres varchar(100),
@Apellidos varchar(100),
@Password varchar(50)
)
AS 
UPDATE [User] 
SET Nombres=@Nombres,
Apellidos=@Apellidos,
[Password]=@Password
WHERE UserId=@UserId
GO
/****** Object:  StoredProcedure [dbo].[spUser_ValidarUsuario]    Script Date: 17/04/2018 9:55:56 p.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Verifica si el Email y password concuerdan con algun usuario en la base de datos
Retorna 1 si existe 
Retorna 0 si no existe
*/
CREATE PROC [dbo].[spUser_ValidarUsuario]
(
@Email varchar (50),
@Password varchar(50)
)
AS 
Declare @result int;

IF EXISTS (SELECT 1 FROM [User] WHERE Email=@Email AND [Password]=@Password)
BEGIN 
set @result=1;
     select @result as result;
END 
ELSE 
BEGIN

set @result=0;
     select @result as result; 
END 
GO
USE [master]
GO
ALTER DATABASE [dbPI] SET  READ_WRITE 
GO
