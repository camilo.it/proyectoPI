﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyectoPI.Controllers
{
    public class UserController : ApiController
    {
        // GET: api/User
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/User/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/User       
        public Object IsRegister([FromBody]Datos.Entities.User user)
        {

            if (ModelState.IsValid)
            {

                if (!string.IsNullOrEmpty(user.Email) && !string.IsNullOrEmpty(user.Password))
                {
                    var result = Datos.Methods.UserMethods.IsRegister(user.Email, user.Password);

                    JObject json = new JObject(
                        new JProperty("status", HttpStatusCode.OK),
                        new JProperty("result", result)
                        );

                    return json;
                }
                else
                {
                    return new JObject(
                        new JProperty("status", HttpStatusCode.BadRequest)
                        );
                }
            }
            else
            {
                return new JObject(
                        new JProperty("status", HttpStatusCode.BadRequest)
                        );
            }
        }

        // PUT: api/User/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
        }
    }
}
