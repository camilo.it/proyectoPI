﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyectoPI.Controllers
{
    public class ClasesController : ApiController
    {
        //// GET: api/Clases
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/Clases/5
        [HttpGet]
        public JObject Get(int cursoId)
        {
            JObject json = new JObject();
            JObject curso = null;

            try
            {
                //Obtiene los cursospendientes
                List<Datos.Entities.Clases> clases = Datos.Methods.ClasesMethods.GetClasesPorCursoId(Convert.ToInt64(cursoId));

                JArray array = new JArray();
                for (int i = 0; i < clases.Count; i++)
                {

                    //crea un objeto curso
                    curso = new JObject();
                    curso.Add(new JProperty("ClasesId", clases[i].ClasesId));
                    curso.Add(new JProperty("CursoId", clases[i].CursoId));
                    curso.Add(new JProperty("LinkVideo", clases[i].LinkVideo));
                    curso.Add(new JProperty("Descripcion", clases[i].Descripcion));
                    curso.Add(new JProperty("Titulo", clases[i].Titulo));
                    curso.Add(new JProperty("Calificacion", clases[i].Calificacion));
                    curso.Add(new JProperty("Fecha_Publicacion", clases[i].Fecha_Publicacion));


                    //Añade el objeto al array
                    array.Add(curso);

                }


                json.Add(new JProperty("statuscode", "200"));
                json.Add(new JProperty("messageStatus", "Completado"));
                json.Add(new JProperty("clases", array));
            }
            catch (Exception ex)
            {
                json.Add(new JProperty("statuscode", "500"));
                json.Add(new JProperty("messageStatus", "Error en la peticion"));
                throw ex;
            }

            return json;
        }

        // POST: api/Clases
        public JObject Post([FromBody]string cursoId)
        {

            JObject json = new JObject();
            JObject curso = null;

            try
            {
                //Obtiene los cursospendientes
                List<Datos.Entities.Clases> clases = Datos.Methods.ClasesMethods.GetClasesPorCursoId(Convert.ToInt64(cursoId));

                JArray array = new JArray();
                for (int i = 0; i < clases.Count; i++)
                {

                    //crea un objeto curso
                    curso = new JObject();
                    curso.Add(new JProperty("ClasesId", clases[i].ClasesId));
                    curso.Add(new JProperty("CursoId", clases[i].CursoId));
                    curso.Add(new JProperty("LinkVideo", clases[i].LinkVideo));
                    curso.Add(new JProperty("Descripcion", clases[i].Descripcion));
                    curso.Add(new JProperty("Titulo", clases[i].Titulo));
                    curso.Add(new JProperty("Calificacion", clases[i].Calificacion));
                    curso.Add(new JProperty("Fecha_Publicacion", clases[i].Fecha_Publicacion));
                    

                    //Añade el objeto al array
                    array.Add(curso);

                }


                json.Add(new JProperty("statuscode", "200"));
                json.Add(new JProperty("messageStatus", "Completado"));
                json.Add(new JProperty("clases", array));
            }
            catch (Exception ex)
            {
                json.Add(new JProperty("statuscode", "500"));
                json.Add(new JProperty("messageStatus", "Error en la peticion"));
                throw ex;
            }

            return json;
        }

        // PUT: api/Clases/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Clases/5
        public void Delete(int id)
        {
        }
    }
}
