﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace proyectoPI.Controllers
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;


    public class CursosController : ApiController
    {
        // GET: api/Cursos
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Cursos/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Cursos        
        public JObject Post()//[FromBody]string value
        {
            JObject json = new JObject();
            JObject curso = null;

            try
            {
                //Obtiene los cursospendientes
                List<Datos.Entities.Cursos> cursos= Datos.Methods.CursosMethods.GetCursosPorRevisar();

                JArray array = new JArray();
                for (int i = 0; i < cursos.Count; i++)
                {
                   
                    //crea un objeto curso
                    curso = new JObject();
                    curso.Add(new JProperty("CursoId", cursos[i].CursoId));
                    curso.Add(new JProperty("Titulo",cursos[i].Titulo));
                    curso.Add(new JProperty("Descripcion", cursos[i].Descripcion));
                    curso.Add(new JProperty("Requisitos", cursos[i].Requisitos));
                    curso.Add(new JProperty("Calificacion", cursos[i].Calificacion));
                    curso.Add(new JProperty("Status", cursos[i].Status));
                    curso.Add(new JProperty("Fecha_Publicacion", cursos[i].Fecha_Publicacion));
                    curso.Add(new JProperty("UserId", cursos[i].UserId));

                    //Añade el objeto al array
                    array.Add(curso);

                }


                json.Add(new JProperty("statuscode", "200"));
                json.Add(new JProperty("messageStatus", "Completado"));
                json.Add(new JProperty("cursos", array));
            }
            catch (Exception ex)
            {
                json.Add(new JProperty("statuscode", "500"));
                json.Add(new JProperty("messageStatus", "Error en la peticion"));
                throw ex;
            }

            return json;
        }

        // PUT: api/Cursos/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Cursos/5
        public void Delete(int id)
        {
        }
    }
}
