﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoPI.Views
{
    public partial class Curso : System.Web.UI.Page
    {
        wsClases.wsClasesSoapClient ws = new wsClases.wsClasesSoapClient();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                try
                {
                    var clasesId = Request.QueryString["clasesId"].ToString();

                    //Obtiene informacion de la clase
                    var clases = ws.GetClase(Seguridad.Seguridad.getToken_Clases(),Convert.ToInt64(clasesId));





                    ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> ReemplazarVideo('"+clases.LinkVideo+"')</script>");
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            else
            {
                Session.RemoveAll();
                Response.Redirect("Home.aspx");
            }
        }
    }
}