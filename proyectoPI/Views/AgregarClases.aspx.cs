﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoPI.Views
{
    public partial class AgregarClases : System.Web.UI.Page
    {
        #region Variables
        wsUser.User user = null;
        wsCurso.Cursos curso = new wsCurso.Cursos();
        #endregion



        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Session["User"] != null)
                    {
                        wsUser.User user = (wsUser.User)Session["User"];                        

                    }
                    else
                    {
                        Session.RemoveAll();
                        Response.Redirect("Home.aspx");
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }



        #endregion



        #region Eventos

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtTitulo.Text) &&
                !string.IsNullOrEmpty(txtDescripcion.Text) &&
                !string.IsNullOrEmpty(txtLink.Text))
            {
                wsClases.Clases clases = new wsClases.Clases();

                clases.Titulo = txtTitulo.Text;
                clases.Descripcion = txtTitulo.Text;
                clases.LinkVideo = txtLink.Text;
                clases.Fecha_Publicacion = DateTime.Now;
                clases.CursoId = curso.CursoId = (long)Session["CursoId"]; 

                wsClases.wsClasesSoapClient wsClasesMethods = new wsClases.wsClasesSoapClient();

                clases = wsClasesMethods.InsertarClase(Seguridad.Seguridad.getToken_Clases(), clases);

                if (clases.ClasesId != 0)
                {
                    string nameDocument = Path.GetFileName(FileUpload1.FileName);
                    if (!string.IsNullOrEmpty(nameDocument))
                    {
                        string Extension = Path.GetExtension(FileUpload1.FileName);
                        if (Extension.Equals(".pdf") ||
                            Extension.Equals(".doc") ||
                            Extension.Equals(".txt") ||
                            Extension.Equals(".ppt") ||
                            Extension.Equals(".pps"))
                        {
                            nameDocument = nameDocument.Replace(Extension, "");
                            nameDocument = nameDocument.Replace(nameDocument, nameDocument + " " + clases.ClasesId + Extension);

                            string rutaDestino = Server.MapPath("~/Storage/" + nameDocument);

                            FileUpload1.SaveAs(rutaDestino);

                            wsDocumentos.Documentos documentos = new wsDocumentos.Documentos();
                            documentos.ClasesId = clases.ClasesId;
                            documentos.Url = rutaDestino;

                            wsDocumentos.wsDocumentosSoapClient wsDocumentosMethods = new wsDocumentos.wsDocumentosSoapClient();

                            documentos = wsDocumentosMethods.InsertarDocumento(Seguridad.Seguridad.getToken_Documentos(), documentos);

                            if (documentos != null)
                            {
                                txtTitulo.Text = string.Empty;
                                txtDescripcion.Text = string.Empty;
                                txtLink.Text = string.Empty;
                                ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Exito!', 'Se Inserto la clase correctamente', 'success')</script>");
                            }


                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Error', 'Extension de Archivo Incorrecta', 'error')</script>");
                        }
                    }


                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Falta Informacion', 'Por favor Llene los campos', 'error')</script>");
                }
            }

        }
        #endregion
    }
}