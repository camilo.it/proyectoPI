﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/_Layout.Master" AutoEventWireup="true" CodeBehind="Curso.aspx.cs" Inherits="proyectoPI.Views.Curso" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Meta-tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Grounding Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
	<script>
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Metga-tags -->

	<!-- Custom Theme files -->
	<link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
	<!-- creative blog css -->

	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<!--// Pop-up -->
	<link href="css/banner.css" rel="stylesheet" type="text/css" media="all" />
	<!--// banner-css -->
	<link rel="stylesheet" href="css/shop.css" type="text/css" media="screen" property="" />
	<!--// shop-css -->
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- common-css -->
	<link href="css/banner.css" rel="stylesheet" type="text/css" media="all" />
	<!--// banner-css -->
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<!-- //pop up box -->

	<!-- services -->
	<link href="css/easy-responsive-tabs1.css" rel='stylesheet' type='text/css' />
	<!-- //services -->
	<link href="css/single.css" type="text/css" rel="stylesheet" media="all">
	<!--// Responsive-tabs -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- font-awesome icons -->
	<!-- //Custom Theme files -->
	<!-- Web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	    rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
	<!-- //Web-fonts -->


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="Scripts/Curso.js" type="text/javascript"></script>


	<div class="demo-main demo-sec">
		<div class="containers">
			<div class="demo-content-left col-md-8">
				<h4 class="demo-title">flexible options
					<span>to fit your schedule</span>
				</h4>
				<!-- <h4><span class="demo-line"></span>e-learning<span class="demo-line"></span></h4> -->
				<div class="demo-left" id="contentVideo">
					<%--<iframe id="frameVideo" width="560" height="315" src="https://www.youtube.com/embed/I75CUdSJifw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>--%>
                    
				</div>
				
				<div class="clearfix"></div>
			</div>
			
			<div class="clearfix"></div>
			
			<div class="demo-inst demo-sec">
				<h6 class="sec-title">about the
					<span>instructor</span>
				</h6>
				<div class="col-md-5 left-instr">
					<div class="col-md-5 demo-inst-left">
						<img src="images/tf1.jpg" class="img-responsive" alt="" />
						<div class="inst-txt">
							<h5>Helen L. Odom</h5>
							<p>designation in detail</p>
						</div>
					</div>
					<div class="col-md-7 demo-inst-center ">
						<ul>
							<li>
								<i class="fa fa-star" aria-hidden="true"></i>
								<span>4.7</span> average rating</li>
							<li>
								<i class="fa fa-comment-o" aria-hidden="true"></i>
								<span>75,000</span> reviews</li>
							<li>
								<i class="fa fa-play" aria-hidden="true"></i>
								<span>19</span> courses</li>
							<li>
								<i class="fa fa-user" aria-hidden="true"></i>
								<span>34,826</span> students</li>

						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-7 demo-inst-right">
					<h6>Best Lead Educator</h6>
					<p>Nunc gravida sagittis nunc, pellentesque vehicula lacus viverra quis. Fusce ultricies velit nibh, ut aliquet elit hendrerit
						ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ullamcorper augue iaculis fermentum ultrices. Nullam
						nec faucibus odio. Donec at dui ut tellus pharetra congue a at tellus. Sed facilisis est et est eleifend, a iaculis
						massa maximus. Pellentesque condimentum lorem maximus justo blandit eleifend.
					</p>
					<a href="#">view more</a>
				</div>

				<div class="clearfix"></div>

			</div>
			<!-- reviews -->
			<div class="demo-sec">
				<div class="reviews col-md-8 col-sm-8">
					<h6 class="sec-title">student
						<span>review</span>
					</h6>
					<!-- review grid -->
					<div class="row blockquote review-item">
						<div class="col-md-3 col-sm-3 col-xs-3 text-center review-left">
							<img class="rounded-circle reviewer" src="images/r.jpg" alt="">
							<div class="caption">
								<small>by
									<a href="#joe">Jones</a>
								</small>
							</div>

						</div>
						<div class="col-md-9 col-sm-9 col-xs-9 review-right">
							<h4>Here goes your review title</h4>
							<ul class="ratebox">
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-half-o" aria-hidden="true"></span>
								</li>
							</ul>
							<p class="review-text">Fusce ultricies velit nibh, ut aliquet elit hendrerit ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ut ullamcorper augue iaculis fermentum ultrices. Nullam nec faucibus odio. Donec at dui ut tellus pharetra congue
								a at tellus.</p>
							<small class="review-date">March 26, 2017</small>
						</div>
					</div>
					<!-- //review grid -->
					<!-- review grid -->
					<div class="row blockquote review-item">
						<div class="col-md-3 col-sm-3 col-xs-3 text-center review-left">
							<img class="rounded-circle reviewer" src="images/r.jpg" alt="">
							<div class="caption">
								<small>by
									<a href="#joe">Rosy</a>
								</small>
							</div>

						</div>
						<div class="col-md-9 col-sm-9 col-xs-9 review-right">
							<h4>Here goes your review title</h4>
							<ul class="ratebox">
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-half-o" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-o" aria-hidden="true"></span>
								</li>
							</ul>
							<p class="review-text">Fusce ultricies velit nibh, ut aliquet elit hendrerit ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ut ullamcorper augue iaculis fermentum ultrices. Nullam nec faucibus odio. Donec at dui ut tellus pharetra congue
								a at tellus.</p>
							<small class="review-date">March 26, 2017</small>
						</div>
					</div>
					<!-- //review grid -->
					<!-- review grid -->
					<div class="row blockquote review-item">
						<div class="col-md-3 col-sm-3 col-xs-3 text-center review-left">
							<img class="rounded-circle reviewer" src="images/r.jpg" alt="">
							<div class="caption">
								<small>by
									<a href="#joe">Jack</a>
								</small>
							</div>

						</div>
						<div class="col-md-9 col-sm-9 col-xs-9 review-right">
							<h4>Here goes your review title</h4>
							<ul class="ratebox">
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-o" aria-hidden="true"></span>
								</li>
							</ul>
							<p class="review-text">Fusce ultricies velit nibh, ut aliquet elit hendrerit ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ut ullamcorper augue iaculis fermentum ultrices. Nullam nec faucibus odio. Donec at dui ut tellus pharetra congue
								a at tellus.</p>
							<small class="review-date">March 26, 2017</small>
						</div>
					</div>
					<!-- //review grid -->
					<!-- review grid -->
					<div class="row blockquote review-item">
						<div class="col-md-3 col-sm-3 col-xs-3 text-center review-left">
							<img class="rounded-circle reviewer" src="images/r.jpg" alt="">
							<div class="caption">
								<small>by
									<a href="#joe">Ben</a>
								</small>
							</div>

						</div>
						<div class="col-md-9 col-sm-9 col-xs-9 review-right">
							<h4>Here goes your review title</h4>
							<ul class="ratebox">
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-o" aria-hidden="true"></span>
								</li>
								<li>
									<span class="fa fa-star-o" aria-hidden="true"></span>
								</li>

							</ul>
							<p class="review-text">Fusce ultricies velit nibh, ut aliquet elit hendrerit ac. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								Ut ullamcorper augue iaculis fermentum ultrices. Nullam nec faucibus odio. Donec at dui ut tellus pharetra congue
								a at tellus.</p>
							<small class="review-date">March 26, 2017</small>
						</div>
					</div>
					<!-- //review grid -->
				</div>
				<!-- //reviews -->
				<!-- Materials -->
				
				<div class="clearfix"></div>
			</div>
			<!-- //Materials-->
		</div>
	</div>
	<!-- content-section-ends-here -->
    

    <script>
        $('.toggle').click(function () {
            // Switches the Icon
            $(this).children('i').toggleClass('fa-pencil');
            // Switches the forms  
            $('.form').animate({
                height: "toggle",
                'padding-top': 'toggle',
                'padding-bottom': 'toggle',
                opacity: "toggle"
            }, "slow");
        });
	</script>
	<!-- //bootstrap-pop-up -->

	<!--pop-up-box -->
	<script src="js/jquery.magnific-popup.js"></script>
	<script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });

        });
	</script>
	<!-- //pop-up-box -->
	<!-- start-smooth-scrolling -->
	<script src="js/move-top.js"></script>
	<script src="js/easing.js"></script>
	<script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
	</script>
	<!-- //end-smooth-scrolling -->

	<!-- smooth-scrolling-of-move-up -->
	<script>
        $(document).ready(function () {
			/*
			var defaults = {
			    containerID: 'toTop', // fading element id
			    containerHoverID: 'toTopHover', // fading element hover id
			    scrollSpeed: 1200,
			    easingType: 'linear' 
			};
			*/

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
	</script>
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smooth-scrolling-of-move-up -->

	<script src="js/easy-responsive-tabs1.js"></script>
	<script>
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('#verticalTab').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true
            });
        });
	</script>

</asp:Content>
