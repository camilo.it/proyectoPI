﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/_Layout.Master" AutoEventWireup="true" CodeBehind="AgregarClases.aspx.cs" Inherits="proyectoPI.Views.AgregarClases" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container">

        <div class="row" style="margin-top:1em;margin-bottom:1em;">
            <asp:Label ID="lblTitulo" runat="server" Text="Titulo de la Clase"></asp:Label>
            <asp:TextBox ID="txtTitulo"  PlaceHolder="Titulo" runat="server" Width="100%"></asp:TextBox>
        </div>

        <div class="row" style="margin-top:1em;margin-bottom:1em;">
            <asp:Label ID="lblDescripcion" runat="server" Text="Descripcion de la Clase"></asp:Label>
            <asp:TextBox ID="txtDescripcion"  PlaceHolder="Descripcion" Height="150px" Width="100%" runat="server"></asp:TextBox>
        </div>

        <div class="row" style="margin-top:1em;margin-bottom:1em;">
            <asp:Label ID="lblLink" runat="server" Text="Iframe del video que se visualizara"></asp:Label>
            <asp:TextBox ID="txtLink"  PlaceHolder="Link Video" Width="100%" runat="server"></asp:TextBox>
        </div>

        <div class="row" style="margin-top:1em;margin-bottom:1em;">
            <asp:FileUpload ID="FileUpload1" Width="100%" runat="server" style="display:inline-block" />
        </div>


        <div class="row" style="margin-top: 1em; margin-bottom: 1em;text-align:center">
            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        </div>

    </div>

</asp:Content>
