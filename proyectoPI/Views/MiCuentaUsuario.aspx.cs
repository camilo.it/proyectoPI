﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoPI.Views
{
    public partial class MiCuentaUsuario : System.Web.UI.Page
    {
        #region Variables
        wsUser.wsUserSoapClient ws = new wsUser.wsUserSoapClient();        
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Session["User"] != null)
                    {
                        wsUser.User user = (wsUser.User)Session["User"];

                        txtNombres.Text = user.Nombres;
                        txtApellidos.Text = user.Apellidos;
                        txtPassword.Text = user.Password;

                        
                    }
                    else
                    {
                        Session.RemoveAll();
                        Response.Redirect("Home.aspx");
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }


        //guardar informacion
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            int contador = 0;
            string mensaje = "";

            if (string.IsNullOrEmpty(txtNombres.Text)){ contador++; mensaje += "Falta Nombres. \n"; }
            if (string.IsNullOrEmpty(txtApellidos.Text)){ contador++; mensaje += "Falta Apellidos. \n"; }
            if (string.IsNullOrEmpty(txtPassword.Text)){ contador++; mensaje += "Falta Password. \n"; }
            if (string.IsNullOrEmpty(txtConfirmarPassword.Text)){contador++; mensaje += "Falta Confirmar Password. \n"; }
            if (txtPassword.Text == txtConfirmarPassword.Text)
            {
                if (contador == 0)
                {
                    try
                    {
                        wsUser.User user = new wsUser.User();
                        user.Nombres = txtNombres.Text;
                        user.Apellidos = txtApellidos.Text;
                        user.Password = txtPassword.Text;
                        wsUser.User userSession = (wsUser.User)Session["User"];
                        user.UserId = userSession.UserId;

                        string result= ws.UpdateUser(Seguridad.Seguridad.getToken_User(), user);

                         ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Actualización', '" + result + "', 'info')</script>");
                        

                    }
                    catch (Exception ex)
                    {
                       throw ex;
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Actualización', '" + mensaje+"', 'info')</script>");
                   
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Actualización', 'El password no coincide', 'info')</script>");
            }

            

        }



        //add curso
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            var user = (wsUser.User)Session["User"];


            if (
                !string.IsNullOrEmpty(txtTitulo.Text) &&
                !string.IsNullOrEmpty(txtDescripcion.Text) &&
                !string.IsNullOrEmpty(txtRequisitos.Text)
                )
            {
                wsCurso.Cursos curso = new wsCurso.Cursos();

                curso.UserId = user.UserId;
                curso.Titulo = txtTitulo.Text;
                curso.Descripcion = txtDescripcion.Text;
                curso.Requisitos = txtRequisitos.Text;

                wsCurso.wsCursosSoapClient wsCursos = new proyectoPI.wsCurso.wsCursosSoapClient();

                curso=wsCursos.InsertarCurso(Seguridad.Seguridad.getToken_Cursos(), curso);

                Session["CursoId"] = curso.CursoId;

                Response.Redirect("AgregarClases.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Falta Informacion', 'Por favor Llene los campos', 'error')</script>");
            }
            




        }
    }
}