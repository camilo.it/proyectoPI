﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoPI.Views
{
    public partial class _Layout : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                btnModalLogin.Visible = false;
                contentLogOut.Visible = true;
            }
            else
            {
                btnModalLogin.Visible = true;
                contentLogOut.Visible = false;
            }

        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            Session.RemoveAll();
            Response.Redirect("Home.aspx");
        }


        protected void btnMiCuenta_Click(object sender, EventArgs e)
        {
            wsUser.User user = new wsUser.User();
            try
            {
                //se capturan los datos del session
                user = (wsUser.User)Session["User"];

                int rol = (int)user.RolId;

                //segun el rol del usuario
                switch (rol)
                {
                    case (int)Datos.Methods.RolMethods.Roles.Suscriptor:
                        Response.Redirect("MiCuentaUsuario.aspx");
                        break;

                    case (int)Datos.Methods.RolMethods.Roles.Moderador:
                        Response.Redirect("MiCuentaModerador.aspx");
                        break;

                    case (int)Datos.Methods.RolMethods.Roles.Administrador:
                        Response.Redirect("");
                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
    }
}