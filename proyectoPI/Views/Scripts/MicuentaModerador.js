﻿'use strict'


$(document).ready(() => {


    //hace la peticion de los cursos
    $.ajax({
        url: '/proyectoPI/api/Cursos',
        method: 'POST',
        contentType: 'application/json'

    })
        .done(function (data) {
            AgregarListaAlDiv(data)
        })
        .fail(function (data) {

        })

});

/**
 * Agrega contenido al Div #ContentCursos
 * @param {any} data
 */
function AgregarListaAlDiv(data)
{
    try {        
        
        for (var i = 0; i < data.cursos.length; i++) {
            $('#ContentCursos').append(
                '<a class="list-group-item" href="CursosRevision.aspx?cursoId=' + data.cursos[i].CursoId + '">' + data.cursos[i].Titulo + '</a>'                
            );
            
        }
      


    } catch (e) {

    }


}