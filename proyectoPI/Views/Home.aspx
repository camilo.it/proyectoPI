﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/_Layout.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="proyectoPI.Views.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!-- banner -->
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1" class=""></li>
			<li data-target="#myCarousel" data-slide-to="2" class=""></li>
			<li data-target="#myCarousel" data-slide-to="3" class=""></li>
			<li data-target="#myCarousel" data-slide-to="4" class=""></li>
		</ol>
		<div class="carousel-inner" role="listbox">
			<div class="item active">
				<div class="container">
					<div class="carousel-caption">
						<p>Cursos</p>
						<h3>Proporcinamos curosos con excelente <span>Calidad.</span></h3>
					</div>
				</div>
			</div>
			<div class="item item2">
				<div class="container">
					<div class="carousel-caption">
						<p>Marketing</p>
						<h3>Cambia tus pensamiento por un <span>Mejor tu.</span></h3>
					</div>
				</div>
			</div>
			<div class="item item3">
				<div class="container">
					<div class="carousel-caption">
						<p>Desarrollo</p>
						<h3>La forma simple de vencer la <span>Postergacion de tus METAS.</span></h3>
					</div>
				</div>
			</div>
			<div class="item item4">
				<div class="container">
					<div class="carousel-caption">
						<p>Business</p>
						<h3>Modernizando,virtualizando su <span>Negocio!</span></h3>
					</div>
				</div>
			</div>
			<div class="item item5">
				<div class="container">
					<div class="carousel-caption">
						<p>Matematicas</p>
						<h3>Logre mas con  <span> Sessiones privadas.</span></h3>
					</div>
				</div>
			</div>
		</div>
		<!-- Arrows -->
		<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
			<span class="fa fa-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previo</span>
		</a>
		<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
			<span class="fa fa-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Siguiente</span>
		</a>
		<!-- //Arrows -->
	</div>
	<!--//banner -->
	<!-- about -->
	<div class="aboutf">
		<div class="container">
			<h3 class="tittlef-agileits-w3layouts">Top de cursos mas populares</h3>
			<p class="paragraphf">Encuentra los mejores cursos, que te ayudaran a conseguir todo lo que te propones.</p>
		</div>
        <div class="container" style="margin-top:1em;margin-bottom:1em;text-align:center">
            
            <asp:TextBox ID="txtBuscar" runat="server" Placeholder="Buscar Curso"></asp:TextBox>
            
            <ajaxToolkit:AutoCompleteExtender
                runat="server"
                ID="autoComplete1"
                TargetControlID="txtBuscar"
                ServiceMethod="GetCursosPorIniciales"
                ServicePath="~/Services/wsCursos.asmx"
                MinimumPrefixLength="2"
                CompletionInterval="1000"
                EnableCaching="true"
                CompletionSetCount="10"
                FirstRowSelected="false"
                UseContextKey="true"
                >     
            </ajaxToolkit:AutoCompleteExtender>
        </div>
		<div class="banner_bottom_tab_grids">
			<div id="verticalTab" class="resp-vtabs" style="display: block; width: 100%; margin: 0px;">
				<ul class="resp-tabs-list">
					<li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab">Cursos</li>
					<li class="resp-tab-item" aria-controls="tab_item-1" role="tab">Marketing</li>
					<li class="resp-tab-item" aria-controls="tab_item-2" role="tab">Desarrollo</li>
					<li class="resp-tab-item" aria-controls="tab_item-3" role="tab">Negocios</li>
					<li class="resp-tab-item" aria-controls="tab_item-4" role="tab">Matematicas</li>
				</ul>
				<div class="resp-tabs-container">
					<!-- tab1 -->
					<div class="resp-tab-content resp-tab-content-active" style="display:block" aria-labelledby="tab_item-0">
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="CursosRevision.aspx?cursoId=1" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Curso Java</h3>
								<p class="paragraphf">Aprende todo sobre como con Java</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="CursosRevision.aspx?cursoId=2" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Andriod</h3>
								<p class="paragraphf">Conoce todas las habilidades necesarias para crear tus aplicaciones </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="courses.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Bases de Datos</h3>
								<p class="paragraphf">onoce el mundo de las bases de datos para entender cómo trabajan, cómo se instalan, y las diferencias entre las distintas marcas (SQL Server, MySQL, Oracle, etc.) Aprende el lenguaje de consultas SQL</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="courses.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Mobile Apps</h3>
								<p class="paragraphf">Aprende a construir apps nativas de forma fácil</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!-- //tabs-info -->
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Network y Seguridad</h3>
								<p class="paragraphf">Aprende a atacar y defender las infraestructuras tecnológicas de las organizaciones, conociendo los distintos tipos de ataques y herramientas que pueden ser utilizados para vulnerar un red.</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Data Science</h3>
								<p class="paragraphf">Aprende las bases que te llevarán a ser un profesional de las ciencias de datos. Maneja grandes cantidades de información. Extrae, clasifica y procesa, y dale valor a estructuras y fuentes de datos. </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Game Development</h3>
								<p class="paragraphf">Si quieres aprender todo sobre el desarrollo de videojuegos, desde cómo se diseñan hasta crear tu primer juego, estos cursos son un gran punto de partida.</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Hardware</h3>
								<p class="paragraphf">              </p>
							</div>
							<div class="clearfix"> </div>

							<!-- // video-button-popup -->
							<div id="small-dialog" class="mfp-hide">
								<iframe src="https://player.vimeo.com/video/7273286"></iframe>
							</div>
							<!-- // video-button-popup -->

						</div>
						<!-- //tabs-info -->
					</div>
					<!-- //tab1 -->
					<!-- tab2 -->
					<div class="resp-tab-content" aria-labelledby="tab_item-1">
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf5.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="marketing.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Digital marketing</h3>
								<p class="paragraphf">        </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="marketing.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Marketing Management</h3>
								<p class="paragraphf">        </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf6.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="marketing.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Social Media Marketing</h3>
								<p class="paragraphf">     </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf7.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="marketing.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Relaciones Publicas</h3>
								<p class="paragraphf">        </p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!-- //tabs-info -->
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog1" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Diseño</h3>
								<p class="paragraphf">           </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog1" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Test</h3>
								<p class="paragraphf">        </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog1" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Public Relations</h3>
								<p class="paragraphf">       </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf7.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog1" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Marketing Management</h3>
								<p class="paragraphf">      </p>
							</div>
							<div class="clearfix"> </div>

							<!-- // video-button-popup -->
							<div id="small-dialog1" class="mfp-hide">
								<iframe src="https://player.vimeo.com/video/55806355"></iframe>
							</div>
							<!-- // video-button-popup -->

						</div>
						<!-- //tabs-info -->
					</div>
					<!--// tab2 -->
					<!-- tab3 -->
					<div class="resp-tab-content" aria-labelledby="tab_item-2">
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="development.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Web Development</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="development.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Mobile Apps</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="development.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Lenguajes de programacion</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="development.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Game Development</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!-- //tabs-info -->
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog2" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Databases</h3>
								<p class="paragraphf">Lorem ipsum</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog2" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Software Testing</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog2" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Software Engineering</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog2" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Herramientas de Desarrollo</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>

							<!-- // video-button-popup -->
							<div id="small-dialog2" class="mfp-hide">
								<iframe src="https://player.vimeo.com/video/53755097"></iframe>
							</div>
							<!-- // video-button-popup -->

						</div>
						<!-- //tabs-info -->
					</div>
					<!--// tab3 -->
					<!-- tab4 -->
					<div class="resp-tab-content" aria-labelledby="tab_item-3">
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="business.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Business Law</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="business.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Project Management</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="business.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">E-Commerce</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="business.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Strategy</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!-- //tabs-info -->
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog3" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Communicacion</h3>
								<p class="paragraphf">Lorem ipsum</p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog3" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Entrepreneurship</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog3" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Finanzas</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog3" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Leadership & Management</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>

							<!-- // video-button-popup -->
							<div id="small-dialog3" class="mfp-hide">
								<iframe src="https://player.vimeo.com/video/198096211"></iframe>
							</div>
							<!-- // video-button-popup -->

						</div>
						<!-- //tabs-info -->
					</div>
					<!--// tab4 -->
					<!-- tab5 -->
					<div class="resp-tab-content" aria-labelledby="tab_item-4">
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf9.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="maths.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Algebra</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bbf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="maths.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Calculo</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf8.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="maths.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Nucle de habilidades Matematicas</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf10.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="maths.html" class="buttonf">Saber Mas</a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Geometria</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<!-- //tabs-info -->
						<!-- tabs-info -->
						<div class="main-topicsf">
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf1.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog4" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Probabilidad y Estadistica</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf2.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog4" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Geometria</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf4.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog4" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Test Taking Skills</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="col-md-3 col-sm-6 tabsf-w3-agileits-grids">
								<img src="images/bf3.jpg" alt="" />
								<div class="img-caption">
									<div class="tabs-inn-info-agileits-w3layouts">
										<a href="#small-dialog4" class="popup-with-zoom-anim buttonf"><span class="fa fa-play-circle-o" aria-hidden="true"></span></a>
									</div>
								</div>
								<h3 class="sub-w3ls-headf">Series y Secuencias</h3>
								<p class="paragraphf">Lorem ipsum </p>
							</div>
							<div class="clearfix"> </div>

							<!-- // video-button-popup -->
							<div id="small-dialog4" class="mfp-hide">
								<iframe src="https://player.vimeo.com/video/110807219?color=c9ff23&byline=0"></iframe>
							</div>
							<!-- // video-button-popup -->

						</div>
						<!-- //tabs-info -->
					</div>
					<!--// tab5 -->
				</div>
			</div>
		</div>
	</div>
	<!-- //about-->

	<!-- creative-animated-loading -->
	<div class="loadf-section">
		<div class="posf-grids">
			<div class="container">
				<h3 class="tittlef-agileits-w3layouts">Logre mas con sesiones de Coaching</h3>
				<div class="col-md-4 posf-left">
					<div class="pos1">
						<a href="courses.html">Courses</a>
						<p class="paragraphf white-clrf">Pellentesque </p>
					</div>
					<div class="pos2">
						<a href="marketing.html">Marketing</a>
						<p class="paragraphf white-clrf">Pellentesque </p>
					</div>
				</div>
				<div id="loader">
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="lading"></div>
				</div>
				<div class="col-md-4 posf-right">
					<div class="pos3">
						<a href="development.html">Development</a>
						<p class="paragraphf white-clrf">Pellentesque convallis diam consequat</p>
					</div>
					<div class="pos4">
						<a href="business.html">Business</a>
						<p class="paragraphf white-clrf">Pellentesque convallis diam consequat</p>
					</div>
				</div>
				<div class="clearfix"> </div>
				<div class="posf-btm">
					<div class="pos5">
						<a href="maths.html">Maths</a>
						<p class="paragraphf white-clrf">Pellentesque convallis diam consequat</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--// creative-animated-loading -->
	<!-- Reviews -->
	<%--<div class="reviewf-main">
		<div class="container">
			<h3 class="tittlef-agileits-w3layouts">Amazing Client Stories</h3>
			<!-- reviewsf-left -->
			<div class="col-md-6 reviewsf-left">
				<div class="reviewsf-left">
					<div class="news-grids-bottom">
						<!-- cycler -->
						<div id="design" class="date">
							<div id="cycler">
								<div class="date-text">
									<img src="images/tf1.jpg" alt=" " class="img-responsive" />
									<div class="clientf-info">
										<span class="fa fa-quote-left" aria-hidden="true"></span>
										<p>Pellentesque convallis diam consequat magna vulputate malesuada. Cras a ornare elit.</p>
										<h6>Elizabeth Harper</h6>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="date-text">
									<img src="images/tf2.jpg" alt=" " class="img-responsive" />
									<div class="clientf-info">
										<span class="fa fa-quote-left" aria-hidden="true"></span>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit amet lectus turpis. </p>
										<h6>Anthony</h6>
									</div>
									<div class="clearfix"> </div>
								</div>
								<div class="date-text">
									<img src="images/tf3.jpg" alt=" " class="img-responsive" />
									<div class="clientf-info">
										<span class="fa fa-quote-left" aria-hidden="true"></span>
										<p>Pellentesque convallis diam consequat magna vulputate malesuada. Cras a ornare elit.</p>
										<h6>Karen smith</h6>
									</div>
									<div class="clearfix"> </div>
								</div>
							</div>
						</div>
						<!-- //cycler -->
					</div>
				</div>
			</div>
			<!-- //reviewsf-left -->
			<div class="col-md-6 reviewsf-right">
				<h3 class="sub-w3ls-headf">Pellentesque convallis diam <span>consequat magna</span> vulputate malesuada</h3>
				<p class="paragraphf"><span class="fa fa-repeat" aria-hidden="true"></span> Pellentesque convallis diam consequat magna vulputate malesuada.
					Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar neque pharetra ac.</p>
				<p class="paragraphf"><span class="fa fa-repeat" aria-hidden="true"></span> Lorem Ipsum convallis diam consequat magna vulputate malesuada.
					Cras a ornare elit. Nulla viverra pharetra sem, eget pulvinar neque pharetra ac.</p>
				<div class="buttonf-styl">
					<a href="contact.html">Contact Us</a>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>--%>
	<!-- //Reviews -->
	<!--//mobile-app -->
	<div class="mobile-app">
		<div class="container">
			<div class="col-md-6 app-info">
				<h3 class="tittlef-agileits-w3layouts">Mobile App</h3>
				<p class="paragraphf">No pares de aprender con nuestra aplicacion.</p>
				<div class="app-devices">
					<a href="#"><img src="images/app.png" alt=""></a>
					<a href="#"><img src="images/app1.png" alt=""></a>
					<div class="clearfix"> </div>
				</div>
				<p class="paragraphf"><a href="#">Click aqui! </a>Saber mas sobre las aplicaciones.</p>
			</div>
		<%--	<div class="col-md-6 app-img">
				<img src="images/screens.png" alt=" " class="img-responsive">
			</div>--%>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--//mobile-app -->

    <!-- bootstrap-pop-up -->
	<div class="modal video-modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					Ingresar & Registrarse
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<section>
					<div class="modal-body">
						<div class="loginf_module">
							<div class="module form-module">
								<div class="toggle"><i class="fa fa-times fa-pencil"></i>
									<div class="tooltip">Click!</div>
								</div>
								<div class="form">
									<h3>Ingrese a su cuenta</h3>
									    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtEmailIng" type="text"  Placeholder="Email" ></asp:TextBox>
										<asp:TextBox runat="server" ClientIDMode="Static" ID="txtPasswordIng" TextMode="Password" Placeholder="Password" ></asp:TextBox>										
                                        <asp:Button ID="btnIngresar" runat="server" type="button" Text="Login" OnClick="btnIngresar_Click" AutoPostBack="true"/>        
																	
								</div>
                                
								<div class="form">
									<h3>Crear Cuenta</h3>									    
										
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtNombres"  Placeholder="Nombres" ></asp:TextBox>
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtApellidos"  Placeholder="Apellidos"></asp:TextBox>
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtEmailReg" TextMode="Email" Placeholder="Email" ></asp:TextBox>
                                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtPasswordReg" TextMode="Password" Placeholder="Password"></asp:TextBox>


										<asp:Button runat="server"  ID="btnRegistrar" Text="Registrar" type="button"  OnClick="btnRegistrar_Click"/>
									
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>

</asp:Content>
