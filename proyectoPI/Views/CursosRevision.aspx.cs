﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace proyectoPI.Views
{
    public partial class CursosRevision : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                 long cursoId=Convert.ToInt64(Request.QueryString["cursoId"].ToString());

                    ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> getClases("+cursoId+")</script>");
                }
                catch (Exception)
                {

                    Response.Redirect("Home.aspx");
                }
                
            }
        }
    }
}