﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/_Layout.Master" AutoEventWireup="true" CodeBehind="MiCuentaUsuario.aspx.cs" Inherits="proyectoPI.Views.MiCuentaUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    	<script type="application/x-javascript">
		addEventListener("load", function () {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}
	</script>
	<!--// Metga-tags -->

	<!-- Styleshets -->
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!--// Bootstrap-css -->
	<link rel="stylesheet" href="css/easy-responsive-tabs.css">
	<!--// Responsive-tabs -->
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
	<!--// Pop-up -->
	<link href="css/banner.css" rel="stylesheet" type="text/css" media="all" />
	<!--// banner-css -->
	<link rel="stylesheet" href="css/shop.css" type="text/css" media="screen" property="" />
	<!--// shop-css -->
	<link rel="stylesheet" type="text/css" href="css/checkout.css">
	<!--// Checkout-css -->
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- common-css -->
	<link rel="stylesheet" type="text/css" href="css/jquery-ui1.css">
	<!-- Range-slider-css -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<!-- // Font-awesome-css -->
	<!--// Styleshets -->

	<!-- Online-fonts -->
	<link href="//fonts.googleapis.com/css?family=Montserrat:300,500,600,700,800" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<!--// Online-fonts -->
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <!--Payment-->
	<div class="innerf-pages">
		<div class="container">
			<div class="privacy about">
				<h3>Pay<span>ment</span></h3>
				<!--/tabs-->
				<div class="responsive_tabs innfpage-tabs">
					<div id="horizontalTab">
						<ul class="resp-tabs-list">											
							<li>Agregar Curso</li>
							<li>Mi Cuenta</li>
						</ul>
						<div class="resp-tabs-container">
							<!--/tab_one-->
							<div class="tab1">
								<div class="pay_info">
							       <div class="row" style="margin-top:2em;">
                                       <div class="col-lg-3 col-md-3 col-xs-12">
                                           <asp:Label ID="lblTitulo" runat="server" Text="Titulo :"></asp:Label>
                                       </div>
                                       <div class="col-lg-9 col-md-9 col-xs-12">
                                           <asp:TextBox ID="txtTitulo" runat="server" style="width:100%" ></asp:TextBox>
                                       </div>                                       
							       </div>
                                    <div class="row" style="margin-top:2em;">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <asp:Label ID="lblDescripcion" runat="server" Text="Descripcion :"></asp:Label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <asp:TextBox TextMode="MultiLine" ID="txtDescripcion" runat="server" style="width:100%;height:100px;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:2em;">
                                        <div class="col-lg-3 col-md-3 col-xs-12">
                                            <asp:Label ID="lblRequisitos" runat="server" Text="Requisitos :"></asp:Label>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-xs-12">
                                            <asp:TextBox TextMode="MultiLine" ID="txtRequisitos" runat="server" style="width:100%;height:100px;"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="row" style="text-align:center; margin-top:2em;" >
                                        <asp:Button ID="btnAdd" runat="server" Text="Agregar Curso"  OnClick="btnAdd_Click"/>
                                    </div>


								</div>

							</div>
							<!--//tab_one-->
							
							<div class="tab2">
								<div class="pay_info">
									<div class="col-md-6 tab-grid">
										
									</div>
									<div class="col-md-6">
										
											<div class="clearfix">
												<div class="form-group form-group-cc-number">
													<label>Nombres</label>
                                                    <asp:TextBox ID="txtNombres" CssClass="form-control" runat="server"></asp:TextBox>
													</div>
												<div class="form-group form-group-cc-cvc">
													<label>Apellidos</label>
                                                    <asp:TextBox ID="txtApellidos" CssClass="form-control" runat="server"></asp:TextBox>
													
												</div>
											</div>
											<div class="clearfix">
												<div class="form-group form-group-cc-name">
													<label>Password</label>
                                                    <asp:TextBox ID="txtPassword" type="password" CssClass="form-control" runat="server"></asp:TextBox>
													
												</div>
												<div class="form-group form-group-cc-date">
													<label>Confirmar Password</label>
                                                    <asp:TextBox ID="txtConfirmarPassword" type="password" runat="server" CssClass="form-control"></asp:TextBox>
													
												</div>
											</div>										
											
                                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary submit" OnClick="btnGuardar_Click" />
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!--//tabs-->
			</div>

		</div>
	</div>
	<!-- //payment -->




	<!-- js -->

	<!-- Common js -->
	<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
	<!--// Common js -->

	<!-- cart-js -->
	<script src="js/minicart.js"></script>
	<script>
		edu.render();

		edu.cart.on('edu_checkout', function (evt) {
			var items, len, i;

			if (this.subtotal() > 0) {
				items = this.items();

				for (i = 0, len = items.length; i < len; i++) {}
			}
		});
	</script>
	<!-- //cart-js -->

	<!-- easy-responsive-tabs -->
	<script src="js/easy-responsive-tabs.js"></script>
	<script>
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
				type: 'default', //Types: default, vertical, accordion           
				width: 'auto', //auto or any width like 600px
				fit: true, // 100% fit in a container
				closed: 'accordion', // Start closed if in accordion view
				activate: function (event) { // Callback function if tab is switched
					var $tab = $(this);
					var $info = $('#tabInfo');
					var $name = $('span', $info);
					$name.text($tab.text());
					$info.show();
				}
			});
		});
	</script>

	<!-- //easy-responsive-tabs -->



	<!-- Nav Js -->
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script>
		$(document).ready(function () {
			$(".dropdown").hover(
				function () {
					$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideDown("400");
					$(this).toggleClass('open');
				},
				function () {
					$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).slideUp("400");
					$(this).toggleClass('open');
				}
			);
		});
	</script>
	<!--// Nav Js -->
	
	<script>
		$('.toggle').click(function () {
			// Switches the Icon
			$(this).children('i').toggleClass('fa-pencil');
			// Switches the forms  
			$('.form').animate({
				height: "toggle",
				'padding-top': 'toggle',
				'padding-bottom': 'toggle',
				opacity: "toggle"
			}, "slow");
		});
	</script>
	<!-- //bootstrap-pop-up -->
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function ($) {
			$(".scroll").click(function (event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop: $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function () {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/

			$().UItoTop({
				easingType: 'easeOutQuart'
			});


           
              
		});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- smoothscroll -->
	<script src="js/SmoothScroll.min.js"></script>
	<!-- //smoothscroll -->


    <%-- Scripts del formulario--%>
    <script src="Scripts/MiCuentaUsuario.js" type="text/javascript" ></script>
    <%-- Scripts del formulario--%>

</asp:Content>
