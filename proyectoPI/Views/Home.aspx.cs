﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services.Protocols;
using proyectoPI.Seguridad;

namespace proyectoPI.Views
{
    public partial class Home : System.Web.UI.Page
    {
        #region Variables
        wsUser.wsUserSoapClient ws = new wsUser.wsUserSoapClient();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["Token"] != null)
                {
                    txtEmailIng.Text = Request.Cookies["Token"].Value;
                }
            }
        }


        #region Eventos
        protected void btnIngresar_Click(object sender, EventArgs e)
        {
           

            if (!string.IsNullOrEmpty(txtEmailIng.Text) && !string.IsNullOrEmpty(txtPasswordIng.Text))
            {
                bool result = ws.IsRegister(Seguridad.Seguridad.getToken_User(),txtEmailIng.Text, txtPasswordIng.Text);

                //Si el usuario ya esta registrado
                if (result)
                {
                    wsUser.User user = ws.GetUser(Seguridad.Seguridad.getToken_User(),txtEmailIng.Text);

                    //Coloca en el session todos los datos del usuario
                    Session["User"] = user;

                    //Añade el email a la cookie
                    HttpCookie cookie = new HttpCookie("Token", txtEmailIng.Text);
                    cookie.Expires = DateTime.Now.AddDays(5);

                    this.Response.Cookies.Add(cookie);

                    txtEmailIng.Text = string.Empty;
                    txtPasswordIng.Text = string.Empty;

                    Response.Redirect("Home.aspx");

                }
                else //Si no esta registrado
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Login', 'Usuario no se encuentra registrado', 'error')</script>");
                }
            }
            else
            {
               
                HttpCookie cookie = new HttpCookie("Token", txtEmailIng.Text);
                cookie.Expires = DateTime.Now.AddDays(-1);
                this.Response.Cookies.Add(cookie);
                ClientScript.RegisterStartupScript(this.GetType(), "Good job", "<script> swal('Login', 'Usuario y Password Incorrectos', 'error')</script>");
                
            }


        }

        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtEmailReg.Text) && 
                !string.IsNullOrEmpty(txtPasswordReg.Text) &&
                !string.IsNullOrEmpty(txtNombres.Text) &&
                !string.IsNullOrEmpty(txtApellidos.Text))
            {
                wsUser.User user = new wsUser.User();
                user.Nombres = txtNombres.Text;
                user.Apellidos = txtApellidos.Text;
                user.Email = txtEmailReg.Text;
                user.Password = txtPasswordReg.Text;

                
                

                string result= ws.InsertarUsuario(Seguridad.Seguridad.getToken_User(),user);
                 
                
                txtEmailReg.Text = string.Empty;
                txtPasswordReg.Text = string.Empty;

                Response.Write("<script>alert('"+result+"')</script>");
            }
        }
        #endregion



        
    }
}