﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace proyectoPI.Seguridad
{
    public static class Seguridad
    {
        #region seguridad
        public static wsUser.Security getToken_User()
        {
            try
            {
                //Instancia del WebServices
                wsUser.wsUserSoapClient ws = new wsUser.wsUserSoapClient();
                ws.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ServerUser"].ToString();
                ws.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ServerPass"].ToString();
                wsUser.Security seguridad = new wsUser.Security()
                {
                    Token = DateTime.Now.ToString("yyyyMMdd")
                };

                string token = ws.AutenticacionUsuario(seguridad);
                if (token.Equals("-1")) throw new Exception("Requiere autenticacion");

                seguridad.AutenticationToken = token;

                return seguridad;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



        public static wsClases.Security getToken_Clases()
        {
            try
            {
                //Instancia del WebServices
                wsClases.wsClasesSoapClient ws = new wsClases.wsClasesSoapClient();
                ws.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ServerUser"].ToString();
                ws.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ServerPass"].ToString();
                wsClases.Security seguridad = new wsClases.Security()
                {
                    Token = DateTime.Now.ToString("yyyyMMdd")
                };

                string token = ws.AutenticacionUsuario(seguridad);
                if (token.Equals("-1")) throw new Exception("Requiere autenticacion");

                seguridad.AutenticationToken = token;

                return seguridad;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        public static wsCurso.Security getToken_Cursos()
        {
            try
            {
                //Instancia del WebServices
                wsCurso.wsCursosSoapClient ws = new wsCurso.wsCursosSoapClient();
                ws.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ServerUser"].ToString();
                ws.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ServerPass"].ToString();
                wsCurso.Security seguridad = new wsCurso.Security()
                {
                    Token = DateTime.Now.ToString("yyyyMMdd")
                };

                string token = ws.AutenticacionUsuario(seguridad);
                if (token.Equals("-1")) throw new Exception("Requiere autenticacion");

                seguridad.AutenticationToken = token;

                return seguridad;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



        public static wsDocumentos.Security getToken_Documentos()
        {
            try
            {
                //Instancia del WebServices
                wsDocumentos.wsDocumentosSoapClient ws = new wsDocumentos.wsDocumentosSoapClient();
                ws.ClientCredentials.UserName.UserName = ConfigurationManager.AppSettings["ServerUser"].ToString();
                ws.ClientCredentials.UserName.Password = ConfigurationManager.AppSettings["ServerPass"].ToString();
                wsDocumentos.Security seguridad = new wsDocumentos.Security()
                {
                    Token = DateTime.Now.ToString("yyyyMMdd")
                };

                string token = ws.AutenticacionUsuario(seguridad);
                if (token.Equals("-1")) throw new Exception("Requiere autenticacion");

                seguridad.AutenticationToken = token;

                return seguridad;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion
    }
}