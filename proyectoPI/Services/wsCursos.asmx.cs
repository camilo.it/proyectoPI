﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace proyectoPI.Services
{
    /// <summary>
    /// Descripción breve de wsCursos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsCursos : System.Web.Services.WebService
    {


        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [WebMethod]
        public List<string> GetCursosPorIniciales(string prefixText,int count)
        {
            return Datos.Methods.CursosMethods.GetCursosPorIniciales(prefixText);
        }
    }
}
