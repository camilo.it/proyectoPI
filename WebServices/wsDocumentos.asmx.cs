﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebServices
{
    /// <summary>
    /// Descripción breve de wsDocumentos
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    [System.Web.Script.Services.ScriptService]
    public class wsDocumentos : System.Web.Services.WebService
    {
        public Datos.MiddleWares.Security SoapHeader;

        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public string AutenticacionUsuario()
        {
            try
            {
                if (SoapHeader == null) return "-1";
                if (!SoapHeader.CredencialesValidas(SoapHeader.Token)) return "-1";


                string token = Guid.NewGuid().ToString();

                HttpRuntime.Cache.Add(
                    token,
                    SoapHeader.Token,
                    null,
                    System.Web.Caching.Cache.NoAbsoluteExpiration,
                    TimeSpan.FromMinutes(30),
                    System.Web.Caching.CacheItemPriority.NotRemovable,
                    null
                    );

                return token;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public Datos.Entities.Documentos InsertarDocumento(Datos.Entities.Documentos documentos)
        {
            try
            {
                if (SoapHeader == null) throw new Exception("Requiere autenticacion");
                if (!SoapHeader.CredencialesValidas(SoapHeader)) throw new Exception("Require autenticacion");

                var result = Datos.Methods.DocumentosMethods.InsertarDocumento(documentos);

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
