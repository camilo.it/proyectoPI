﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace WebServices
{
    /// <summary>
    /// Descripción breve de wsUser
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class wsUser : System.Web.Services.WebService
    {
        public Datos.MiddleWares.Security SoapHeader;

        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public string AutenticacionUsuario()
        {
            try
            {
                if (SoapHeader == null) return "-1";
                if (!SoapHeader.CredencialesValidas(SoapHeader.Token)) return "-1";


                string token = Guid.NewGuid().ToString();

                HttpRuntime.Cache.Add(
                    token,
                    SoapHeader.Token,
                    null,
                    System.Web.Caching.Cache.NoAbsoluteExpiration,
                    TimeSpan.FromMinutes(30),
                    System.Web.Caching.CacheItemPriority.NotRemovable,
                    null
                    );

                return token;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// Actualiza la informacion basica de un usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public string UpdateUser(Datos.Entities.User user)
        {
            try
            {
                if (SoapHeader == null) throw new Exception("Requiere autenticacion");
                if (!SoapHeader.CredencialesValidas(SoapHeader)) throw new Exception("Require autenticacion");

                string result = Datos.Methods.UserMethods.UpdateUser(user);

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        /// <summary>
        /// Registra un nuevo usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public string InsertarUsuario(Datos.Entities.User user)
        {
            try
            {
                return Datos.Methods.UserMethods.InsertarUsuario(user);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        /// <summary>
        /// Valida si un usuario ya se encuentra registrado
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public bool IsRegister(string Email, string password)
        {
            try
            {
                if (SoapHeader == null) throw new Exception("Requiere validacion");

                if (!SoapHeader.CredencialesValidas(SoapHeader)) throw new Exception("Requiere validacion");

                return Datos.Methods.UserMethods.IsRegister(Email, password);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        /// <summary>
        /// Retorna toda la informacion de un usuario segun su email
        /// de lo contrario retorna un null
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [WebMethod]
        [System.Web.Services.Protocols.SoapHeader("SoapHeader")]
        public Datos.Entities.User GetUser(string email)
        {
            try
            {
                return Datos.Methods.UserMethods.GetUser(email);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
